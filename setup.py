#!/usr/bin/python3

from setuptools import find_packages
from setuptools import setup

setup(
    name='gitlab-maestro',
    version='0.1.dev0',
    description='Simple orchestration for GitLab servers.',
    author='Florian Preinstorfer',
    author_email='fp@xell.at',
    url='https://gitlab.com/xell/gitlab-maestro',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3 (AGPLv3)'
        'Operating System :: POSIX',
        'Topic :: Utilities',
    ],
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'click',
        'python-gitlab',
        'pyyaml',
    ],
    entry_points='''
        [console_scripts]
        gitlab-maestro=gitlabmaestro.cli:cli
    ''',
)

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 smartindent autoindent
