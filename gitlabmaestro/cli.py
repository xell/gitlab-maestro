#!/usr/bin/python3

import click

from gitlabmaestro.tasks import GitlabMaestro


# Groups
@click.group()
@click.option('--token', required=True, envvar='GITLAB_TOKEN',
              help='The GitLab private token.')
@click.option('--url', default='https://git/', envvar='GITLAB_URL',
              help='The GitLab server URL.')
@click.option('--verbosity', default='INFO', type=click.Choice(
              ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']),
              envvar='GITLAB_DEBUG')
@click.pass_context
def cli(ctx, token, url, verbosity):
    ctx.obj = GitlabMaestro(token, url, verbosity)


# Groups
@cli.group(help='Various listings from the GitLab server.')
def list():
    pass


# Commands per group
@list.command(help='List all unmanaged projects/groups.')
@click.argument('conffiles', type=click.Path(exists=True), nargs=-1)
@click.pass_obj
def unmanaged(gm, conffiles):
    gm.connect()
    gm.load_conffiles(conffiles)

    projects = sorted(gm.get_unmanaged_projects())
    groups = sorted(gm.get_unmanaged_groups())
    for part_str, part_obj in (('projects', projects), ('groups', groups)):
        if part_obj:
            click.echo('{} unmanaged {}:'.format(len(part_obj), part_str))
            for item in part_obj:
                click.echo(' * {}'.format(item))
        else:
            click.echo('There are no unmanaged {}.'.format(part_str))


@cli.command(help='Update the configuration on the GitLab server.')
@click.argument('conffiles', type=click.Path(exists=True), nargs=-1)
@click.pass_obj
def update(gm, conffiles):
    gm.connect()
    gm.load_conffiles(conffiles)
    gm.update()

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 smartindent autoindent
