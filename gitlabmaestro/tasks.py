#!/usr/bin/python3

import logging
import urllib.parse

import gitlab
import yaml

logging.basicConfig()


class GitlabMaestro(object):
    # Yaml configuration keys
    KEY_GROUPS = 'groups'
    KEY_PROJECTS = 'projects'
    KEY_USER = 'user'
    KEY_USER_ID = 'user_id'
    KEY_PERMISSION = 'permission'
    KEY_PERMISSION_ID = 'permission_id'
    KEY_MEMBERS = 'members'

    # Those (top level) keys are actually processd.
    ALLOWED_CONFIG_KEYS = (KEY_GROUPS, KEY_PROJECTS)

    def __init__(self, token, url, verbosity):
        self._token = token
        self._url = url

        self._log = logging.getLogger(
            '.'.join((__name__, self.__class__.__name__)))
        self._log.setLevel(verbosity)

        self._connection = None
        self._config = dict({key: {} for key in self.ALLOWED_CONFIG_KEYS})

    # Properties
    @property
    def url(self):
        """Get the GitLab url."""
        return self._url

    @property
    def connection(self):
        """Get the connection to GitLab."""
        if self._connection is None:
            self._log.debug('No connection established yet.')
            self.connect()
        return self._connection

    # Public methods
    def connect(self):
        """Connect with GitLab."""
        self._log.debug(
            'Establishing connection to GitLab server: "{}"'.format(self._url))
        self._connection = gitlab.Gitlab(self._url, self._token)
        self._connection.auth()
        self._log.debug('Connection established.')

    def load_conffiles(self, conffiles):
        """Load configuration files and load missing settings from GitLab."""
        self._log.debug('Loading config files: {}'.format(','.join(conffiles)))

        # Load and merge configuration files.
        for conffile in conffiles:
            self._load_and_merge_conffile(conffile)

        # Resolve convenience strings to IDs.
        self._resolve_member_permissions()
        self._resolve_user_id()
        for key in self.ALLOWED_CONFIG_KEYS:
            self._log.debug('Loaded and resolved {} {}.'.
                            format(len(self._config[key]), key))

    def get_unmanaged_projects(self):
        """Get a list of unmanaged projects."""
        unmanaged = []
        for project in self.connection.projects.list(all=True):
            if project.path_with_namespace not in \
                    self._config[self.KEY_PROJECTS]:
                unmanaged.append(project.path_with_namespace)
        return unmanaged

    def get_unmanaged_groups(self):
        """Get a list of unmanaged groups."""
        unmanaged = []
        for group in self.connection.groups.list(all=True):
            if group.path not in self._config[self.KEY_GROUPS]:
                unmanaged.append(group.path)
        return unmanaged

    def update(self):
        """Apply configuration updates to GitLab."""
        self._update_groups()
        self._update_projects()

    # Private methods
    def _load_and_merge_conffile(self, conffile):
        """Load configuration files."""
        with open(conffile) as f:
            full = yaml.load(f)
            for key in self.ALLOWED_CONFIG_KEYS:
                self._config[key].update(full.get(key, {}))

    def _resolve_member_permissions(self):
        """Resolve member permissions to IDs."""
        self._log.debug('Resolving member permission levels.')
        for key in self.ALLOWED_CONFIG_KEYS:
            for item in self._config[key]:
                if self.KEY_MEMBERS not in self._config[key][item]:
                    self._config[key][item][self.KEY_MEMBERS] = {}

                for member in self._config[key][item][self.KEY_MEMBERS]:
                    member[self.KEY_PERMISSION_ID] = getattr(
                        gitlab.Group,
                        '{}_ACCESS'.format(member[self.KEY_PERMISSION]))

    def _resolve_user_id(self):
        """Resolve user names to user IDs."""
        self._log.debug('Resolving user IDs.')

        # Query the GitLab user list
        users = {}
        for user in self.connection.users.list(all=True):
            users[user.username] = user.id

        # Update each configuration part.
        for key in self.ALLOWED_CONFIG_KEYS:
            for item in self._config[key]:
                if self.KEY_MEMBERS not in self._config[key][item]:
                    self._config[key][item][self.KEY_MEMBERS] = {}
                for member in self._config[key][item][self.KEY_MEMBERS]:

                    try:
                        member[self.KEY_USER_ID] = users[member[self.KEY_USER]]
                    except KeyError:
                        self._log.error("No user with username '{}' found.".
                                        format(member[self.KEY_USER]))
                        raise

    def _update_groups(self):
        """Run all group tasks."""
        tasks = [task for task in dir(self) if task.startswith('_task_group_')]

        # Process each group.
        for group in self.connection.groups.list(all=True):
            if group.path in self._config[self.KEY_GROUPS]:
                self._log.info("Processing group '{}'".format(group.path))
                for task in tasks:
                    func = getattr(self, task)
                    func(group, self._config[self.KEY_GROUPS][group.path])

    def _update_projects(self):
        """Run all project tasks."""
        tasks = [
            task for task in dir(self) if task.startswith('_task_project_')]

        # Process each group.
        for group in self.connection.groups.list(all=True):
            if group.path in self._config[self.KEY_PROJECTS]:
                self._log.info("Processing group '{}'".format(group.path))
                for task in tasks:
                    func = getattr(self, task)
                    func(group, self._config[self.KEY_PROJECTS][group.path])

        # Process each project.
        for project_key in self._config[self.KEY_PROJECTS]:
            quoted_project_key = urllib.parse.quote(project_key, safe="")
            try:
                project = self.connection.projects.get(quoted_project_key)
                self._log.info("Processing project '{}'".format(project_key))
                for task in tasks:
                    func = getattr(self, task)
                    func(project, self._config[self.KEY_PROJECTS][project_key])
            except gitlab.GitlabGetError:
                self._log.warning("The project '{}' does not exist.".format(
                    project_key))

    # Group tasks.
    def _task_group_members(self, group, items):
        """Update group members."""
        self._log.debug('Update group members.')
        to_process = {
            user[self.KEY_USER_ID]: user for user in items[self.KEY_MEMBERS]}
        for member in group.members.list(all=True):
            # Delete superflous members.
            if member.id not in to_process:
                self._log.info(
                    "Deleting superflous group member: '{}' (ID: {}).".format(
                        member.username, member.id))
                member.delete()
            # Delete members with wrong permission (update is not implemented).
            elif member.access_level != \
                    to_process[member.id][self.KEY_PERMISSION_ID]:
                self._log.info(
                    "Deleting group member with permission mismatch: '{}' "
                    "(ID: {}).".format(member.username, member.id))
                member.delete()
            # Member is up-to-date and needs no further processing.
            else:
                del to_process[member.id]

        # Add missing members.
        for value in to_process.values():
            self._log.info(
                "Adding new group member '{}' with level '{}'.".
                format(value[self.KEY_USER], value[self.KEY_PERMISSION]))
            group.members.create(
                {"access_level": value[self.KEY_PERMISSION_ID],
                 "user_id": value[self.KEY_USER_ID]})

    def _task_project_members(self, project, items):
        """Update project members."""
        self._log.debug('Update project members.')
        to_process = {
            user[self.KEY_USER_ID]: user for user in items[self.KEY_MEMBERS]}
        for member in project.members.list(all=True):
            # Delete superflous members.
            if member.id not in to_process:
                self._log.info(
                    "Deleting superflous project member: '{}' (ID: {}).".
                    format(member.username, member.id))
                member.delete()
            # Delete members with wrong permission (update is not implemented).
            elif member.access_level != \
                    to_process[member.id][self.KEY_PERMISSION_ID]:
                self._log.info(
                    "Deleting project member with permission mismatch: '{}' "
                    "(ID: {}).".format(member.username, member.id))
                member.delete()
            # Member is up-to-date and needs no further processing.
            else:
                del to_process[member.id]

        # Add missing members.
        for value in to_process.values():
            self._log.info(
                "Adding new project member '{}' with level '{}'.".
                format(value[self.KEY_USER], value[self.KEY_PERMISSION]))
            project.members.create(
                {"access_level": value[self.KEY_PERMISSION_ID],
                 "user_id": value[self.KEY_USER_ID]})

    def _task_project_settings(self, project, items):
        """Apply project settings."""
        self._log.debug('Update project settings.')
        for k, v in items['settings'].items():
            setattr(project, k, v)
        project.save()

    def _task_project_protect_branches(self, project, items):
        """Protect the listed branches of a project."""
        def get_settings_from_items(items, name):
            d = {}
            try:
                for k in ('developers_can_push', 'developers_can_merge'):
                    d[k] = items['protected_branches'][name].get(k, False)
            except KeyError:
                pass
            return d

        self._log.debug('Protect project branches.')
        for branch in project.branches.list(all=True):
            item_settings = get_settings_from_items(items, branch.name)
            # Remove superflous branch protection.
            if branch.protected:
                if branch.name not in items['protected_branches'].keys():
                    self._log.info("Unprotect branch: '{}'.".
                                   format(branch.name))
                    branch.unprotect()
                else:
                    branch.protect(**item_settings)
            # Add missing branch protection.
            else:
                if branch.name in items['protected_branches'].keys():
                    self._log.info(
                        "Protecting branch: '{}'.".format(branch.name))
                    branch.protect(**item_settings)

    def _task_project_webhooks(self, project, items):
        """Update webhooks for a project."""
        # Delete all non-specified hooks.
        self._log.debug('Update project webhooks.')
        requested_hooks = {item['url']: item for item in items['webhooks']}
        for hook in project.hooks.list(all=True):
            if hook.url not in requested_hooks:
                self._log.info("Deleting superflous webhook: '{}'.".
                               format(hook.url))
                hook.delete()
            else:
                del requested_hooks[hook.url]

        # Create the remaining requested hooks.
        for url, hook in requested_hooks.items():
            self._log.info("Creating new webhook: '{}'.".format(url))
            project.hooks.create(hook)

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 smartindent autoindent
