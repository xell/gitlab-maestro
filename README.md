# Gitlab-maestro
Gitlab-maestro is a simple orchestration for GitLab servers. It uses YAML
configuration files to configure group and related project settings on private
GitLab instances.

**Important notice:** This project is in a very early stage of development.
Dragons ahead.

## License
GNU Affero General Public License v3 (AGPLv3)

## Features
* Manage group related settings:
  * Members
* Manage project related settings:
  * Features: Issues, Merge requests, Builds, Wiki, Snippets
  * Default branch
  * Webhooks
  * Projected branches
  * Members

Please take a look at the files provided in examples/ folder. Those files
should help you to get started.

## Installation
Gitlab-maestro uses python-gitlab to interact with a GitLab server. To quickly
get it up and running, install it in a virtual environment for Python 3.

Create a fresh virtualenv and install the project:
```shell
$ virtualenv --python python3 ~/gitlab-venv
$ . ~/gitlab-venv/bin/activate
$ pip install ~/path/to/gitlab-maestro
```

## Upgrade
In case you want to upgrade an existing virtual environment, use the following
commands:
```shell
$ . ~/gitlab-venv/bin/activate
$ pip install --upgrade ~/path/to/gitlab-maestro
```

## One-time-setup
### API token
A private token or a personal access token is required to access a GitLab
instance. You can obtain your private token from the [GitLab account
page](https://gitlab.example.com/profile/account) and a private access token
from the [GitLab Access Tokens
page](https://gitlab.example.com/profile/personal_access_tokens).

### Self-signed TLS certificates
In order to use a self-signed TLS certificate, install it in your local OS
certificate store. For a detailed description refer to
https://nblock.org/2016/07/28/using-self-signed-certificates/

## Usage
Below are a few use cases to get started with gitlab-maestro.

* Update all project settings:
  ```shell
  $ gitlab-maestro --token <YOURTOKEN> update examples/projects.yaml
  ```

* Update all settings:
  ```shell
  $ gitlab-maestro --token <YOURTOKEN> update examples/groups.yaml examples/projects.yaml
  ```

* List all unmanaged projects and groups:
  ```shell
  $ gitlab-maestro --token <YOURTOKEN> list unmanaged examples/groups.yaml examples/projects.yaml
  ```
